#!/usr/bin/bash
## Lab 3 Task 03
## Write a script that takes in two numbers and performs the given operation.
## Add(+) Sub(-) Mult(*) Div(/) Exp(**) Mod(%) 
echo "Please select one of the following: "
echo "add sub mul div exp mod "
read -p "Enter the operator: " op
if [ $op = "add" ]
then
	read -p "Enter the first number: " num1
	read -p "Enter the second number: " num2
	sum=$((num1 + num2))
	echo "The sum of $num1 and $num2 is $sum"
elif [ $op = "sub" ]
then
	read -p "Enter the first number: " num1
        read -p "Enter the second number: " num2
	sub=$((num1 - num2))
	echo "The sub of $num1 and $num2 is $sub"
elif [ $op = "mul" ]
then
        read -p "Enter the first number: " num1
        read -p "Enter the second number: " num2
        mul=$((num1 * num2))
        echo "The mul of $num1 and $num2 is $mul"
elif [ $op = "div" ]
then
        read -p "Enter the first number: " num1
        read -p "Enter the second number: " num2
        div=$((num1 / num2))
        echo "The div of $num1 and $num2 is $div"
elif [ $op = "exp" ]
then
        read -p "Enter the first number: " num1
        read -p "Enter the second number: " num2
	exp=$((num1 ** num2))
        echo "The exp of $num1 and $num2 is $exp"
elif [ $op = "mod" ]
then
        read -p "Enter the first number: " num1
        read -p "Enter the second number: " num2
        mod=$((num1 % num2))
        echo "The mod of $num1 and $num2 is $mod"

fi

