#!/usr/bin/bash
## Lab 3 Task 04
## Make a loop that keeps accepting integers and adding them all together
## $* to grab all integers together
sum=0
for i in $*
do
	sum=$(($i + $sum))
done
	echo "$sum"


