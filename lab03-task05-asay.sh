#!/usr/bin/bash
## Lab 3 Task 05
## Write a script that takes an operator then performs that operation to
## handle multiple integers on the same line
if [ $1="add" ]
then
	sum=0
	for i in $*
	do
		sum=$(($i + $sum))
	done
	echo $sum
elif [ $1="sub" ]
then
        sub=0
        for i in $*
        do
                sub=$(($i - $sub))
        done
        echo $sub
elif [ $1="mul" ]
then
        mul=0
        for i in $*
        do
                mul=$(($i * $mul))
        done
        echo $mul
elif [ $1="div" ]
then
        div=1
        for i in $*
        do
                div=$(($i / $div))
        done
        echo $div
elif [ $1="exp" ]
then
        exp=0
        for i in $*
        do
               exp=$(($i ** $exp))
        done
        echo $exp
elif [ $1="mod" ]
then
        mod=0
        for i in $*
        do
                mod=$(($i % $mod))
        done
        echo $mod
fi

