#!/usr/bin/bash
## Lab 3 Task 01
## Write a bash script that takes the following inputs and outputs the
## the following information in the given format
read -p "Enter your name: " name
read -p "Enter the date: " date
read -p "Enter your city: " city
read -p "Enter the current temperature: " temp
read -p "Enter today's birthday: " birthday
read -p "Enter your payable bills: " bills

echo "Good day, $name. Today is $date in the city of $city. It is a beautiful Tuesday, the temperature is $temp degrees. Today is $birthday's birthday. Insurance is payable, as are the water, gas, and light bills for a total of \$$bills"
